package hms.pojos;

import java.util.Date;
import java.util.List;

/**
 *
 * @author user
 */
public class Doctor extends User {

    private String address;
    private Double experience;
    private Profession profession;
    private List<Date> availableTimes;
    private Long user_id;
    private Long doctorId;

    public Long getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Long doctorId) {
        this.doctorId = doctorId;
    }

    public Long getUser_id() {
        return user_id;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getExperience() {
        return experience;
    }

    public void setExperience(Double experience) {
        this.experience = experience;
    }

    public Profession getProfession() {
        return profession;
    }

    public void setProfession(Profession profession) {
        this.profession = profession;
    }

    public List<Date> getAvailableTimes() {
        return availableTimes;
    }

    public void setAvailableTimes(List<Date> availableTimes) {
        this.availableTimes = availableTimes;
    }

    @Override
    public String toString() {
        //  return doctorId+" "+user.getName()+" "+user.getSurname();
        return doctorId + " " + name + " " + surname;
    }

}
