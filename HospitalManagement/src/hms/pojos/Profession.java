package hms.pojos;

import java.util.List;

/**
 *
 * @author user
 */
public class Profession {
    private Long id;
    private String work;
    private List<Doctor>doctors;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getWork() {
        return work;
    }

    public void setWork(String work) {
        this.work = work;
    }

    public List<Doctor> getDoctors() {
        return doctors;
    }

    public void setDoctors(List<Doctor> doctors) {
        this.doctors = doctors;
    }

    @Override
    public String toString() {
        return work;
    }
    
}
