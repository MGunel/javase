package hms.pojos;

/**
 *
 * @author GUNEL
 */
public class Picture {
    private int id;
    private String imgName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImgName() {
        return imgName;
    }

    public void setImgName(String imgName) {
        this.imgName = imgName;
    }
    
}
