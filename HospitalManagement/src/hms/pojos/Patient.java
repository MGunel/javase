package hms.pojos;

/**
 *
 * @author user
 */
public class Patient extends User{
    private Long user_id;
    private String address;
    private Long patientId;

    public Long getPatientId() {
        return patientId;
    }

    public void setPatientId(Long patientId) {
        this.patientId = patientId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getUser_id() {
        return user_id;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    @Override
    public String toString() {
        return "Patient{" + "user_id=" + user_id + ", address=" + address + ", patientId=" + patientId + '}';
    }

    
    

    
    
}
