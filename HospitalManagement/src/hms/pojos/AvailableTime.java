package hms.pojos;

import java.util.Date;

/**
 *
 * @author GUNEL
 */
public class AvailableTime {

    private Long at_id;
    private Date available_date;
    private Doctor doctor;

    public Long getAt_id() {
        return at_id;
    }

    public void setAt_id(Long at_id) {
        this.at_id = at_id;
    }

    public Date getAvailable_date() {
        return available_date;
    }

    public void setAvailable_date(Date available_date) {
        this.available_date = available_date;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    @Override
    public String toString() {
        return available_date+"" ;
    }

}
