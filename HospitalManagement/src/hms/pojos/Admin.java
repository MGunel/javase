package hms.pojos;

/**
 *
 * @author user
 */
public class Admin extends User{
    private Long adminId;
    private Long user_id;

    public Long getAdminId() {
        return adminId;
    }

    public void setAdminId(Long adminId) {
        this.adminId = adminId;
    }

    public Long getUser_id() {
        return user_id;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }
    
}
