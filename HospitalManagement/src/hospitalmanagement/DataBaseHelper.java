package hospitalmanagement;

import hms.pojos.Admin;
import hms.pojos.AvailableTime;
import hms.pojos.Doctor;
import hms.pojos.Gender;
import hms.pojos.Patient;
import hms.pojos.Picture;
import hms.pojos.Profession;
import hms.pojos.Request;
import hms.pojos.RequestStatus;
import hms.pojos.User;
import hms.pojos.UserStatus;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 *
 * @author user
 */
public class DataBaseHelper {

    public Connection connection;
    public PreparedStatement statement;
    public ResultSet resultSet;

    public void connect() throws IOException, ClassNotFoundException, SQLException {
        Properties properties = new Properties();

        try (InputStream input = DataBaseHelper.class.getClassLoader().getResourceAsStream("hospitalmanagement/datasource.properties")) {
            properties.load(input);
            String user = properties.getProperty("USER");
            String password = properties.getProperty("PASSWORD");
            String driverName = properties.getProperty("DRIVER");
            String url = properties.getProperty("URL");
            Class.forName(driverName);
            connection = DriverManager.getConnection(url, user, password);
            System.out.println("ACCESS");

        }
    }

    public void connect(boolean commit) throws IOException, ClassNotFoundException, SQLException {
        Properties properties = new Properties();

        try (InputStream input = DataBaseHelper.class.getClassLoader().getResourceAsStream("hospitalmanagement/datasource.properties")) {
            properties.load(input);
            String user = properties.getProperty("USER");
            String password = properties.getProperty("PASSWORD");
            String driverName = properties.getProperty("DRIVER");
            String url = properties.getProperty("URL");
            Class.forName(driverName);
            connection = DriverManager.getConnection(url, user, password);
            connection.setAutoCommit(commit);
            System.out.println("ACCESS");

        }
    }

    public void disconnect() throws SQLException {
        if (resultSet != null) {
            resultSet.close();
        }
        if (statement != null) {
            statement.close();
        }
        if (connection != null) {
            connection.close();
        }
        System.out.println("Closed");
    }

    public void register(User user) throws IOException, ClassNotFoundException, SQLException {
        connect(false);
        Long id = registerUser(user);
        if (user instanceof Patient) {
            registerPatient((Patient) user, id);
        } else if (user instanceof Doctor) {
            registerDoctor((Doctor) user, id);
        } else {
            registerAdmin((Admin) user, id);
        }
        connection.commit();
        disconnect();
    }

    private Long registerUser(User user) throws SQLException {
        String query = "INSERT INTO USERS"
                + "(USERNAME,PASSWORD,NAME,SURNAME,STATUS,GENDER,AGE,CONTACT) "
                + "values(?,?,?,?,?,?,?,?)";
        statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        statement.setString(1, user.getUsername());
        statement.setString(2, user.getPassword());
        statement.setString(3, user.getName());
        statement.setString(4, user.getSurname());
        statement.setString(5, user.getStatus().name());
        statement.setString(6, user.getGender().name());
        statement.setInt(7, user.getAge());
        //statement.setString(7, user.getImage());
        statement.setString(8, user.getContact());
        statement.execute();
        resultSet = statement.getGeneratedKeys();
        Long newRowId = -1L;
        if (resultSet.next()) {
            newRowId = resultSet.getLong(1);
        }
        if (newRowId == -1L) {
            throw new SQLException("can not read id from user");
        }
        return newRowId;
    }

    private void registerDoctor(Doctor doctor, Long id) throws SQLException {
        String query = "INSERT INTO DOCTORS (ADDRESS,EXPERIENCE,PROFESSION_ID,USER_ID) "
                + " VALUES "
                + " (?,?,?,?)";
        statement = connection.prepareStatement(query);
        statement.setString(1, doctor.getAddress());
        statement.setDouble(2, doctor.getExperience());
        statement.setLong(3, doctor.getProfession().getId());
        statement.setLong(4, id);
        statement.execute();
    }

    private void registerPatient(Patient patient, Long id) throws SQLException {
        String query = "INSERT INTO PATIENT (ADDRESS,USER_ID)values(?,?)";
        statement = connection.prepareStatement(query);
        statement.setString(1, patient.getAddress());
        statement.setLong(2, id);
        statement.execute();
    }

    private void registerAdmin(Admin admin, Long id) throws SQLException {
        String query = "INSERT INTO ADMINS (USER_ID)values(?)";
        statement = connection.prepareStatement(query);
        statement.setLong(1, id);
        statement.execute();
    }

    public List<Profession> ListWork() throws ClassNotFoundException, SQLException, IOException {
        List<Profession> mylist;
        connect();
        mylist = new ArrayList<>();
        try {
            String query = "select * from profession";
            statement = connection.prepareStatement(query);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Profession profession = new Profession();
                profession.setId(resultSet.getLong(1));
                profession.setWork(resultSet.getString(2));
                mylist.add(profession);

            }
        } finally {
            disconnect();
        }
        return mylist;

    }

    public User checkUsername(String username) throws ClassNotFoundException, SQLException, IOException {
        connect();
        try {
            String query = "select * from users where username=?";
            statement = connection.prepareStatement(query);
            statement.setString(1, username);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                User u = new User();
                u.setId(resultSet.getLong(1));
                u.setUsername(resultSet.getString(2));
                u.setPassword(resultSet.getString(3));
                u.setName(resultSet.getString(4));
                u.setSurname(resultSet.getString(5));
                u.setStatus(UserStatus.valueOf(resultSet.getString(6)));
                u.setGender(Gender.valueOf(resultSet.getString(7)));
                u.setAge(resultSet.getInt(8));
                u.setImage(resultSet.getString(9));
                u.setContact(resultSet.getString(10));
                return u;

            } else {
                return null;
            }

        } finally {
            disconnect();
        }

    }

    public User checkPassword(String password) throws ClassNotFoundException, SQLException, IOException {
        connect();
        try {
            String query = "select * from users where password=?";
            statement = connection.prepareStatement(query);
            statement.setString(1, password);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                User u = new User();
                u.setId(resultSet.getLong(1));
                u.setUsername(resultSet.getString(2));
                u.setPassword(resultSet.getString(3));
                u.setName(resultSet.getString(4));
                u.setSurname(resultSet.getString(5));
                u.setStatus(UserStatus.valueOf(resultSet.getString(6)));
                u.setGender(Gender.valueOf(resultSet.getString(7)));
                u.setAge(resultSet.getInt(8));
                u.setImage(resultSet.getString(9));
                u.setContact(resultSet.getString(10));
                return u;

            } else {
                return null;
            }

        } finally {
            disconnect();
        }

    }

    public Doctor doctorLoadPage(String username) throws IOException, ClassNotFoundException, SQLException {
        connect();
        try {
            String query = "SELECT "
                    + "    u.id, "
                    + "    u.username, "
                    + "    u.password, "
                    + "    u.name, "
                    + "    u.surname, "
                    + "    u.status,  "
                    + "    u.gender, "
                    + "    u.age, "
                    + "    u.image, "
                    + "    u.contact, "
                    + "    d.address, "
                    + "    d.experience, "
                    + "    d.profession_id "
                    + " FROM "
                    + "    users u "
                    + "        JOIN "
                    + "    doctors d ON (u.id = d.user_id) "
                    + " WHERE "
                    + "    username = ?";
            statement = connection.prepareStatement(query);
            statement.setString(1, username);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                Doctor d = new Doctor();
                d.setId(resultSet.getLong(1));
                d.setUsername(resultSet.getString(2));
                d.setPassword(resultSet.getString(3));
                d.setName(resultSet.getString(4));
                d.setSurname(resultSet.getString(5));
                d.setStatus(UserStatus.valueOf(resultSet.getString(6)));
                d.setGender(Gender.valueOf(resultSet.getString(7)));
                d.setAge(resultSet.getInt(8));
                d.setImage(resultSet.getString(9));
                d.setContact(resultSet.getString(10));
                d.setAddress(resultSet.getString(11));
                d.setExperience(resultSet.getDouble(12));
                Profession p = new Profession();
                p.setId(resultSet.getLong(13));
                d.setProfession(p);
                return d;
            } else {
                return null;
            }

        } finally {
            disconnect();
        }
    }

    public Doctor findDoctorByUsername(String username) throws SQLException {
        try {
            connect();
            String query = "SELECT "
                    + " * "
                    + " FROM "
                    + " users u "
                    + " JOIN "
                    + " doctors d ON (u.id = d.user_id) "
                    + " JOIN "
                    + " profession p ON (d.profession_id  = p.id) "
                    + " WHERE "
                    + " username = ?";
            statement = connection.prepareStatement(query);
            statement.setString(1, username);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                Doctor doctor = new Doctor();
                doctor.setId(resultSet.getLong(1));
                doctor.setUsername(resultSet.getString(2));
                doctor.setPassword(resultSet.getString(3));
                doctor.setName(resultSet.getString(4));
                doctor.setSurname((resultSet.getString(5)));
                doctor.setStatus(UserStatus.valueOf(resultSet.getString(6)));
                doctor.setGender(Gender.valueOf(resultSet.getString(7)));
                doctor.setAge(resultSet.getInt(8));
                doctor.setImage(resultSet.getString(9));
                doctor.setContact(resultSet.getString(10));
                doctor.setDoctorId(resultSet.getLong(11));
                doctor.setAddress(resultSet.getString(12));
                doctor.setExperience(resultSet.getDouble(13));
                doctor.setUser_id(resultSet.getLong(15));
                Profession profession = new Profession();
                profession.setId(resultSet.getLong(16));
                profession.setWork(resultSet.getString(17));
                doctor.setProfession(profession);
                return doctor;
            }
        } catch (IOException | ClassNotFoundException | SQLException e) {
            e.printStackTrace(System.err);
        } finally {
            disconnect();
        }
        return null;
    }

    public Patient findPatientByUsername(String username) throws SQLException {
        try {
            connect();
            String query = " SELECT "
                    + " * "
                    + " FROM "
                    + " users u "
                    + " JOIN "
                    + " patient p ON (u.id = p.user_id) "
                    + " WHERE "
                    + " username = ? ";
            statement = connection.prepareStatement(query);
            statement.setString(1, username);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                Patient patient = new Patient();
                patient.setId(resultSet.getLong(1));
                patient.setUsername(resultSet.getString(2));
                patient.setPassword(resultSet.getString(3));
                patient.setName(resultSet.getString(4));
                patient.setSurname((resultSet.getString(5)));
                patient.setStatus(UserStatus.valueOf(resultSet.getString(6)));
                patient.setGender(Gender.valueOf(resultSet.getString(7)));
                patient.setAge(resultSet.getInt(8));
                patient.setImage(resultSet.getString(9));
                patient.setContact(resultSet.getString(10));
                patient.setPatientId(resultSet.getLong(11));
                patient.setAddress(resultSet.getString(12));
                patient.setUser_id(resultSet.getLong(13));
                return patient;
            }
        } catch (IOException | ClassNotFoundException | SQLException e) {
            e.printStackTrace(System.err);
        } finally {
            disconnect();
        }
        return null;
    }

    public Admin findAdminByUsername(String username) throws SQLException {
        try {
            connect();
            String query = "SELECT "
                    + " * "
                    + " FROM "
                    + " users u "
                    + " JOIN "
                    + " admins a ON (u.id = a.user_id) "
                    + " WHERE "
                    + " u.username = ?";
            statement = connection.prepareStatement(query);
            statement.setString(1, username);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                Admin admin = new Admin();
                admin.setId(resultSet.getLong(1));
                admin.setUsername(resultSet.getString(2));
                admin.setPassword(resultSet.getString(3));
                admin.setName(resultSet.getString(4));
                admin.setSurname((resultSet.getString(5)));
                admin.setStatus(UserStatus.valueOf(resultSet.getString(6)));
                admin.setGender(Gender.valueOf(resultSet.getString(7)));
                admin.setAge(resultSet.getInt(8));
                admin.setImage(resultSet.getString(9));
                admin.setContact(resultSet.getString(10));
                admin.setAdminId(resultSet.getLong(11));
                admin.setUser_id(resultSet.getLong(12));
                return admin;
            }
        } catch (IOException | ClassNotFoundException | SQLException e) {
            e.printStackTrace(System.err);
        } finally {
            disconnect();
        }
        return null;
    }

    public Patient patientLoadPage(String username) throws IOException, ClassNotFoundException, SQLException {
        connect();
        try {
            String query = "SELECT "
                    + "    u.id, "
                    + "    u.username, "
                    + "    u.password, "
                    + "    u.name, "
                    + "    u.surname, "
                    + "    u.status,  "
                    + "    u.gender, "
                    + "    u.age, "
                    + "    u.image, "
                    + "    u.contact, "
                    + "    p.address "
                    + " FROM "
                    + "    users u "
                    + "        JOIN "
                    + "    patient p ON (u.id = p.user_id) "
                    + " WHERE "
                    + "    username = ?";
            statement = connection.prepareStatement(query);
            statement.setString(1, username);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                Patient p = new Patient();
                p.setId(resultSet.getLong(1));
                p.setUsername(resultSet.getString(2));
                p.setPassword(resultSet.getString(3));
                p.setName(resultSet.getString(4));
                p.setSurname(resultSet.getString(5));
                p.setStatus(UserStatus.valueOf(resultSet.getString(6)));
                p.setGender(Gender.valueOf(resultSet.getString(7)));
                p.setAge(resultSet.getInt(8));
                p.setImage(resultSet.getString(9));
                p.setContact(resultSet.getString(10));
                p.setAddress(resultSet.getString(11));
                return p;
            } else {
                return null;
            }

        } finally {
            disconnect();
        }
    }

    public void insertRequest(Request request) throws SQLException {
        try {
            connect();
            String insertQuery = "insert into request "
                    + " (patient_id,doctor_id,reason,appointment_date) "
                    + " values "
                    + " (?,?,?,?)";
            statement = connection.prepareStatement(insertQuery);
            statement.setLong(1, request.getPatient().getPatientId());
            System.out.println(request.getPatient().getPatientId());
            statement.setLong(2, request.getDoctor().getDoctorId());
            System.out.println("!" + request.getDoctor().getId());
            System.out.println("!" + request.getDoctor().getDoctorId());
            statement.setString(3, request.getReason());
            System.out.println(request.getReason());
            //System.out.println("Before date is" + request.getAppointmentDate());
//            java.sql.Date created = new java.sql.Date(request.getAppointmentDate().getTime());
//            statement.setDate(4, created);
//            System.out.println(created);
            statement.setTimestamp(4, (Timestamp) request.getAppointmentDate());
            statement.execute();
        } catch (IOException | ClassNotFoundException | SQLException e) {
            e.printStackTrace(System.err);
        } finally {
            disconnect();
        }
    }

    public void updateRequest(Request request) throws SQLException {
        try {
            String query = "update  request set status=? where id=?";
            connect();
            statement = connection.prepareStatement(query);
            statement.setString(1, request.getStatus().name());
            statement.setLong(2, request.getRequestId());
            statement.execute();

        } catch (IOException | ClassNotFoundException | SQLException e) {
            e.printStackTrace(System.err);
        } finally {
            disconnect();
        }

    }

    public List<Doctor> findDoctorsByProfessionId(Long profession_id) throws SQLException {
        String query = "SELECT "
                + " * "
                + " FROM "
                + " doctors d "
                + " JOIN "
                + " profession p ON (d.profession_id = p.id) "
                + " JOIN "
                + " users u ON (d.user_id = u.id) "
                + " WHERE "
                + " profession_id = ?";
        List<Doctor> doctors = new ArrayList<>();
        try {
            connect();
            statement = connection.prepareStatement(query);
            statement.setLong(1, profession_id);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Doctor doctor = new Doctor();
                doctor.setDoctorId(resultSet.getLong(1));
                doctor.setAddress(resultSet.getString(2));
                doctor.setExperience(resultSet.getDouble(3));
                //******************Profession*********************
                Profession profession = new Profession();
                profession.setId(resultSet.getLong(6));
                profession.setWork(resultSet.getString(7));
                doctor.setProfession(profession);
                //*****************User******************************
                doctor.setId(resultSet.getLong(8));
                doctor.setUsername(resultSet.getString(9));
                doctor.setPassword(resultSet.getString(10));
                doctor.setName(resultSet.getString(11));
                doctor.setSurname(resultSet.getString(12));
                doctor.setStatus(UserStatus.valueOf(resultSet.getString(13)));
                doctor.setGender(Gender.valueOf(resultSet.getString(14)));
                doctor.setAge(resultSet.getInt(15));
                doctor.setImage(resultSet.getString(16));
                doctor.setContact(resultSet.getString(17));

                doctors.add(doctor);
            }
            return doctors;
        } catch (IOException | ClassNotFoundException | SQLException e) {
            e.printStackTrace(System.err);
        } finally {
            disconnect();
        }
        return null;
    }

    public List<AvailableTime> findAvailableTimesByDoctorId(Long doctorId) throws SQLException {
        try {
            String query = "SELECT "
                    + " * "
                    + " FROM "
                    + " available_times a "
                    + " JOIN "
                    + " doctors d ON (a.doctor_id = d.id) "
                    + " WHERE "
                    + " a.doctor_id = ?;";
            connect();
            statement = connection.prepareStatement(query);
            statement.setLong(1, doctorId);
            resultSet = statement.executeQuery();
            List<AvailableTime> times = new ArrayList<>();
            while (resultSet.next()) {
                AvailableTime availableTime = new AvailableTime();
                availableTime.setAt_id(resultSet.getLong(1));
                availableTime.setAvailable_date(resultSet.getTimestamp(2));
                //****************Doctor**************************
                Doctor doctor = new Doctor();
                doctor.setDoctorId(resultSet.getLong(4));
                doctor.setAddress(resultSet.getString(5));
                doctor.setExperience(resultSet.getDouble(6));
                Profession p = new Profession();
                p.setId(resultSet.getLong(7));
                doctor.setProfession(p);
                doctor.setUser_id(resultSet.getLong(8));
                //************************************************
                availableTime.setDoctor(doctor);
                times.add(availableTime);
            }
            return times;

        } catch (IOException | ClassNotFoundException | SQLException e) {
            e.printStackTrace(System.err);
        } finally {
            disconnect();
        }
        return null;
    }

    public List<Request> findAllRequestsByDoctorId(Long doctorId) throws SQLException {
        String query = "SELECT "
                + "    * "
                + "FROM "
                + "    request r "
                + "        JOIN "
                + "    patient p ON (r.patient_id = p.id) "
                + "        JOIN "
                + "    users u ON (p.user_id=u.id)"
                + "WHERE "
                + "    r.doctor_id = ?";
        try {
            connect();
            statement = connection.prepareStatement(query);
            statement.setLong(1, doctorId);
            resultSet = statement.executeQuery();
            List<Request> requests = new ArrayList<>();
            while (resultSet.next()) {
                Request request = new Request();
                request.setRequestId(resultSet.getLong(1));
                Doctor doctor = new Doctor();
                doctor.setDoctorId(resultSet.getLong(3));
                request.setDoctor(doctor);
                request.setStatus(RequestStatus.valueOf(resultSet.getString(4)));
                request.setReason(resultSet.getString(5));
                request.setAppointmentDate(resultSet.getTimestamp(6));
                Patient patient = new Patient();
                patient.setPatientId(resultSet.getLong(7));
                patient.setAddress(resultSet.getString(8));
                patient.setUser_id(resultSet.getLong(9));
                patient.setUsername(resultSet.getString(11));
                patient.setPassword(resultSet.getString(12));
                patient.setName(resultSet.getString(13));
                patient.setSurname(resultSet.getString(14));
                patient.setStatus(UserStatus.valueOf(resultSet.getString(15)));
                patient.setGender(Gender.valueOf(resultSet.getString(16)));
                patient.setAge(resultSet.getInt(17));
                patient.setImage(resultSet.getString(18));
                patient.setContact(resultSet.getString(19));
                request.setPatient(patient);
                requests.add(request);
            }
            return requests;
        } catch (IOException | ClassNotFoundException | SQLException e) {
            e.printStackTrace(System.err);
        } finally {
            disconnect();
        }
        return null;
    }

    public List<Request> findAllRequestsByPatientId(Long patientId) throws SQLException {
        String query = "SELECT  * FROM request r"
                + " JOIN doctors d ON (r.doctor_id=d.id) "
                + " JOIN users u ON (d.user_id=u.id)"
                + " JOIN profession p ON(d.profession_id=p.id) "
                + " WHERE"
                + "  r.patient_id = ?";
        try {
            connect();
            statement = connection.prepareStatement(query);
            statement.setLong(1, patientId);
            resultSet = statement.executeQuery();
            List<Request> requests = new ArrayList<>();
            while (resultSet.next()) {
                Request request = new Request();
                request.setRequestId(resultSet.getLong(1));
                Doctor doctor = new Doctor();
                doctor.setDoctorId(resultSet.getLong(3));
                doctor.setUsername(resultSet.getString(13));
                doctor.setName(resultSet.getString(15));
                doctor.setSurname(resultSet.getString(16));
                doctor.setStatus(UserStatus.valueOf(resultSet.getString(17)));
                doctor.setGender(Gender.valueOf(resultSet.getString(18)));
                doctor.setContact(resultSet.getString(21));
                Profession profession = new Profession();
                profession.setId(resultSet.getLong(22));
                profession.setWork(resultSet.getString(23));
                doctor.setProfession(profession);
                request.setDoctor(doctor);
                request.setStatus(RequestStatus.valueOf(resultSet.getString(4)));
                request.setAppointmentDate(resultSet.getTimestamp(6));
                Patient patient = new Patient();
                patient.setPatientId(resultSet.getLong(2));

                request.setPatient(patient);
                requests.add(request);
            }
            return requests;
        } catch (IOException | ClassNotFoundException | SQLException e) {
            e.printStackTrace(System.err);
        } finally {
            disconnect();
        }
        return null;
    }

    private Long findUser_id(String user) throws IOException, ClassNotFoundException, SQLException {

        Long user_id = -1L;

        String selectquerry = "select * from users where username=?";
        statement = connection.prepareStatement(selectquerry);
        statement.setString(1, user);
        resultSet = statement.executeQuery();

        if (resultSet.next()) {
            user_id = resultSet.getLong(1);

        }
        if (user_id == -1L) {
            throw new SQLException("can not read id from user");
        }

        return user_id;
    }

    public void update(User user) throws IOException, ClassNotFoundException, SQLException {
        connect(false);

//        Long id = findUser_id(DoctorHomePageController.page.getTxtUsername().getText());
//        updateUser(user, id);
        if (user instanceof Doctor) {
            Long id = findUser_id(DoctorHomePageController.page.getTxtUsername().getText());
            updateUser(user, id);
            updateDoctor((Doctor) user, id);
        }
        if (user instanceof Patient) {
            Long id = findUser_id(PatientHomePageController.page.getTxtUsername().getText());
            updateUser(user, id);
            updatePatient((Patient) user, id);
        }
        if (user instanceof Admin) {
            Long id = findUser_id(DoctorHomePageController.page.getTxtUsername().getText());
            updateUser(user, id);
            updateAdmin((Admin) user, id);
        }
        connection.commit();
        disconnect();
    }

    private void updateUser(User user, Long id) throws IOException, ClassNotFoundException, SQLException {
        if (user.getImage() != null) {

            String querry = " UPDATE USERS "
                    + " SET "
                    + " USERNAME = ?, "
                    + " PASSWORD = ?, "
                    + " NAME = ?, "
                    + " SURNAME = ?, "
                    + " GENDER = ?, "
                    + " AGE = ?, "
                    + " IMAGE = ?, "
                    + " CONTACT = ? "
                    + " WHERE "
                    + " ID = ? ";
            statement = connection.prepareStatement(querry);

            statement.setString(1, user.getUsername());
            statement.setString(2, user.getPassword());
            statement.setString(3, user.getName());
            statement.setString(4, user.getSurname());
            statement.setString(5, user.getGender().name());
            statement.setInt(6, user.getAge());
            statement.setString(7, user.getImage());
            statement.setString(8, user.getContact());
            statement.setLong(9, id);
            statement.execute();
        } else {
            String querry = " UPDATE USERS "
                    + " SET "
                    + " USERNAME = ?, "
                    + " PASSWORD = ?, "
                    + " NAME = ?, "
                    + " SURNAME = ?, "
                    + " GENDER = ?, "
                    + " AGE = ?, "
                    + " CONTACT = ? "
                    + " WHERE "
                    + " ID = ? ";
            statement = connection.prepareStatement(querry);

            statement.setString(1, user.getUsername());
            statement.setString(2, user.getPassword());
            statement.setString(3, user.getName());
            statement.setString(4, user.getSurname());
            statement.setString(5, user.getGender().name());
            statement.setInt(6, user.getAge());
            statement.setString(7, user.getContact());
            statement.setLong(8, id);
            statement.execute();
        }

    }

    private void updateDoctor(Doctor doctor, Long id) throws IOException, ClassNotFoundException, SQLException {

        String querry = " UPDATE DOCTORS "
                + " SET "
                + " ADDRESS = ?, "
                + " EXPERIENCE = ?, "
                + " PROFESSION_ID = ? "
                + " WHERE "
                + " USER_ID = ? ";
        statement = connection.prepareStatement(querry);

        statement.setString(1, doctor.getAddress());
        statement.setDouble(2, doctor.getExperience());
        statement.setLong(3, doctor.getProfession().getId());
        statement.setLong(4, id);
        statement.execute();

    }

    private void updatePatient(Patient patient, Long id) throws SQLException {
        String updateQuery = " UPDATE PATIENT "
                + " SET "
                + " ADDRESS = ? "
                + " WHERE "
                + " USER_ID = ? ";
        statement = connection.prepareStatement(updateQuery);

        statement.setString(1, patient.getAddress());
        statement.setLong(2, id);
        statement.execute();
    }

    private void updateAdmin(Admin admin, Long id) {
    }

    public Picture dbShowPicture(String username) throws IOException, ClassNotFoundException, SQLException {
        connect();
        try {
            String query = "select * from users where username=?";
            statement = connection.prepareStatement(query);
            statement.setString(1, username);
            resultSet = statement.executeQuery();
            Picture picture = new Picture();
            if (resultSet.next()) {
                picture.setId(resultSet.getInt(1));
                picture.setImgName(resultSet.getString(2));
                return picture;
            } else {
                return null;
            }
        } finally {
            disconnect();
        }
    }

}
