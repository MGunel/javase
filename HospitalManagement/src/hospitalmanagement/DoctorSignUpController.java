package hospitalmanagement;

import com.jfoenix.controls.JFXRadioButton;
import hms.pojos.Doctor;
import hms.pojos.Gender;
import hms.pojos.Profession;
import hms.pojos.UserStatus;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author user
 */
public class DoctorSignUpController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        fillComboBox();

    }
    @FXML
    private TextField txtUsername;

    @FXML
    private PasswordField txtPassword;

    @FXML
    private TextField txtName;

    @FXML
    private TextField txtSurname;

    @FXML
    private TextField txtContact;

    @FXML
    private TextField txtAddress;

    @FXML
    private TextField txtExperience;

    @FXML
    private JFXRadioButton  rdbMale;

    @FXML
    private JFXRadioButton  rdbFemale;

    @FXML
    private TextField txtAge;

    @FXML
    private ComboBox<Profession> cmbWork;

    @FXML
    private TextField txtProffession;
   

    //ObservableList<String>olist=FXCollections.observableArrayList();
    @FXML
    public void registerAll() {
        Doctor doctor = new Doctor();
        doctor.setName(txtName.getText());
        doctor.setSurname(txtSurname.getText());
        doctor.setUsername(txtUsername.getText());
        doctor.setPassword(txtPassword.getText());
        doctor.setStatus(UserStatus.ACTIVE);
        if (rdbMale.isSelected()) {
            doctor.setGender(Gender.MALE);
        }
        if (rdbFemale.isSelected()) {
            doctor.setGender(Gender.FEMALE);
        }
        doctor.setAge(Integer.parseInt(txtAge.getText()));
        doctor.setContact(txtContact.getText());
        doctor.setAddress(txtAddress.getText());
        doctor.setExperience(Double.parseDouble(txtExperience.getText()));
        Profession prf = cmbWork.getSelectionModel().getSelectedItem();
        doctor.setProfession(prf);
        DataBaseHelper dh = new DataBaseHelper();
        try {
            dh.register(doctor);
        } catch (IOException | ClassNotFoundException | SQLException ex) {
            ex.printStackTrace(System.err);
        }

    }

    public void fillComboBox() {
        try {
            DataBaseHelper dh = new DataBaseHelper();
            List<Profession> list = dh.ListWork();
            for (Profession work : list) {
                cmbWork.getItems().addAll(work);
            }

        } catch (IOException | ClassNotFoundException | SQLException ex) {
            ex.printStackTrace(System.err);
        }
    }

}
