package hospitalmanagement;

import hms.pojos.Patient;
import hms.pojos.Request;
import java.net.URL;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * FXML Controller class
 *
 * @author GUNEL
 */
public class ShowRequestsByPatientController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        loadDateToTable();
        tblSubmittedRequestTime.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                fillOtherInformation();
            }
        });
    }

    @FXML
    private TableColumn<Request, Date> columnAppointmentDate;

    @FXML
    private TableView<Request> tblSubmittedRequestTime;

    @FXML
    private Label lblDoctorProff;

    @FXML
    private Label lblContact;

    @FXML
    private Label lblEmail;

    @FXML
    private Label lblStatus;

    @FXML
    private Label lblDoctorFullName;

    public static PatientLoginController page = (PatientLoginController) HospitalManagement.yaddas.get("patientLoginPage");

    public void loadDateToTable() {
        try {
            DataBaseHelper dh = new DataBaseHelper();
            Patient patient = dh.findPatientByUsername(page.getTxtUsername().getText());
            System.out.println(patient);
            Long patientId = patient.getPatientId();
            System.out.println(patientId);
            ObservableList<Request> dateList = FXCollections.observableArrayList(dh.findAllRequestsByPatientId(patientId));
            columnAppointmentDate.setCellValueFactory(new PropertyValueFactory<>("appointmentDate"));
            tblSubmittedRequestTime.setItems(null);
            tblSubmittedRequestTime.setItems(dateList);
        } catch (SQLException ex) {
            ex.printStackTrace(System.err);
        }
    }

    public void fillOtherInformation() {
        lblDoctorFullName.setText(tblSubmittedRequestTime.getSelectionModel().getSelectedItem().getDoctor().getName()
                + " " + tblSubmittedRequestTime.getSelectionModel().getSelectedItem().getDoctor().getSurname());
        lblStatus.setText(tblSubmittedRequestTime.getSelectionModel().getSelectedItem().getStatus().name());

        if (lblStatus.getText().equals("PENDING")) {

            lblStatus.setStyle("-fx-text-fill:#5180cc");
        } else if (lblStatus.getText().equals("ACCEPTED")) {

            lblStatus.setStyle("-fx-text-fill:green");
        } else if (lblStatus.getText().equals("CANCELLED")) {

            lblStatus.setStyle("-fx-text-fill:red");
        }
        lblDoctorProff.setText(tblSubmittedRequestTime.getSelectionModel()
                .getSelectedItem()
                .getDoctor()
                .getProfession()
                .getWork());
        lblContact.setText(tblSubmittedRequestTime.getSelectionModel()
                .getSelectedItem()
                .getDoctor().getContact());
        lblEmail.setText(tblSubmittedRequestTime.getSelectionModel()
                .getSelectedItem()
                .getDoctor().getUsername());
    }

    @FXML
    public void logOut(ActionEvent event) {
        try {
            ((Node) event.getSource()).getScene().getWindow().hide();

        } catch (Exception e) {
            e.printStackTrace(System.err);
        }
    }
}
