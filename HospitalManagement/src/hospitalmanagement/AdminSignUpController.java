package hospitalmanagement;

import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTextField;
import hms.pojos.Admin;
import hms.pojos.Gender;
import hms.pojos.UserStatus;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ToggleGroup;

/**
 * FXML Controller class
 *
 * @author GUNEL
 */
public class AdminSignUpController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }
    @FXML
    private JFXTextField txtUsername;

    @FXML
    private JFXTextField txtSurname;

    @FXML
    private JFXTextField txtContact;

    @FXML
    private JFXTextField txtName;

    @FXML
    private JFXRadioButton rdbMale;

    @FXML
    private JFXRadioButton rdbFemale;

    @FXML
    private JFXTextField txtAge;

    @FXML
    private ToggleGroup GenderGroup;

    @FXML
    private JFXTextField txtPassword;

    @FXML
    public void registerAll() {
        Admin admin = new Admin();
        admin.setName(txtName.getText());
        admin.setSurname(txtSurname.getText());
        admin.setUsername(txtUsername.getText());
        admin.setPassword(txtPassword.getText());
        admin.setStatus(UserStatus.BLOCKED);
        if (rdbMale.isSelected()) {
            admin.setGender(Gender.MALE);
        }
        if (rdbFemale.isSelected()) {
            admin.setGender(Gender.FEMALE);
        }
        admin.setAge(Integer.parseInt(txtAge.getText()));
        admin.setContact(txtContact.getText());

        DataBaseHelper dh = new DataBaseHelper();
        try {
            dh.register(admin);
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Message");
            alert.setContentText("Registered successfully but you won't enter to system"
                    + " until your account approved by admins");
            alert.showAndWait();
        } catch (IOException | ClassNotFoundException | SQLException ex) {
            ex.printStackTrace(System.err);
        }

    }
}
