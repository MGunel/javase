package hospitalmanagement;

import hms.pojos.Admin;
import hms.pojos.Doctor;
import hms.pojos.Gender;
import hms.pojos.Patient;
import hms.pojos.Profession;
import hms.pojos.UserStatus;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author GUNEL
 */
public class AdminHomePageController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        fillComboBox();
    }
    @FXML
    private TextField txtUsernameAdmin;

    @FXML
    private TextField txtSurnameAdmin;

    @FXML
    private TextField txtContactAdmin;

    @FXML
    private TextField txtNameAdmin;

    @FXML
    private RadioButton rdbMaleAdmin;

    @FXML
    private RadioButton rdbFemaleAdmin;

    @FXML
    private TextField txtAgeAdmin;

    @FXML
    private TextField txtPasswordAdmin;

    @FXML
    private TextField txtUsernameDoctor;

    @FXML
    private TextField txtSurnameDoctor;

    @FXML
    private TextField txtContactDoctor;

    @FXML
    private TextField txtNameDoctor;

    @FXML
    private RadioButton rdbMaleDoctor;

    @FXML
    private RadioButton rdbFemaleDoctor;

    @FXML
    private TextField txtAgeDoctor;

    @FXML
    private TextField txtAddressDoctor;

    @FXML
    private TextField txtExperienceDoctor;

    @FXML
    private TextField txtPasswordDoctor;

    @FXML
    private ComboBox<Profession> cmbProfessionDoctor;

    @FXML
    private TextField txtUsernamePatient;

    @FXML
    private TextField txtSurnamePatient;

    @FXML
    private TextField txtContactPatient;

    @FXML
    private TextField txtNamePatient;

    @FXML
    private TextField txtAddressPatient;

    @FXML
    private RadioButton rdbMalePatient;

    @FXML
    private RadioButton rdbFemalePatient;

    @FXML
    private TextField txtAgePatient;

    @FXML
    private PasswordField txtPasswordPatient;

    @FXML
    public void commonRegisterAdmin() {
        Admin admin = new Admin();
        admin.setName(txtNameAdmin.getText());
        admin.setSurname(txtSurnameAdmin.getText());
        admin.setUsername(txtUsernameAdmin.getText());
        admin.setPassword(txtPasswordAdmin.getText());
        admin.setStatus(UserStatus.BLOCKED);
        if (rdbMaleAdmin.isSelected()) {
            admin.setGender(Gender.MALE);
        }
        if (rdbFemaleAdmin.isSelected()) {
            admin.setGender(Gender.FEMALE);
        }
        admin.setAge(Integer.parseInt(txtAgeAdmin.getText()));
        admin.setContact(txtContactAdmin.getText());

        DataBaseHelper dh = new DataBaseHelper();
        try {
            dh.register(admin);
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Message");
            alert.setContentText("Registered successfully but you won't enter to system"
                    + " until your account approved by admins");
            alert.showAndWait();
        } catch (IOException | ClassNotFoundException | SQLException ex) {
            ex.printStackTrace(System.err);
        }

    }

    @FXML
    public void commonRegisterDoctor() {
        Doctor doctor = new Doctor();
        doctor.setName(txtNameDoctor.getText());
        doctor.setSurname(txtSurnameDoctor.getText());
        doctor.setUsername(txtUsernameDoctor.getText());
        doctor.setPassword(txtPasswordDoctor.getText());
        doctor.setStatus(UserStatus.ACTIVE);
        if (rdbMaleDoctor.isSelected()) {
            doctor.setGender(Gender.MALE);
        }
        if (rdbFemaleDoctor.isSelected()) {
            doctor.setGender(Gender.FEMALE);
        }
        doctor.setAge(Integer.parseInt(txtAgeDoctor.getText()));
        doctor.setContact(txtContactDoctor.getText());
        doctor.setAddress(txtAddressDoctor.getText());
        doctor.setExperience(Double.parseDouble(txtExperienceDoctor.getText()));
        Profession prf = cmbProfessionDoctor.getSelectionModel().getSelectedItem();
        doctor.setProfession(prf);
        DataBaseHelper dh = new DataBaseHelper();
        try {
            dh.register(doctor);
        } catch (IOException | ClassNotFoundException | SQLException ex) {
            ex.printStackTrace(System.err);
        }

    }

    @FXML
    public void commonRegisterPatient() {
        Patient patient = new Patient();
        patient.setName(txtNamePatient.getText());
        patient.setSurname(txtSurnamePatient.getText());
        patient.setUsername(txtUsernamePatient.getText());
        patient.setPassword(txtPasswordPatient.getText());
        patient.setStatus(UserStatus.ACTIVE);

        if (rdbMalePatient.isSelected()) {
            patient.setGender(Gender.MALE);
        }
        if (rdbFemalePatient.isSelected()) {
            patient.setGender(Gender.FEMALE);
        }
        patient.setAge(Integer.parseInt(txtAgePatient.getText()));
        patient.setContact(txtContactPatient.getText());
        patient.setAddress(txtAddressPatient.getText());
        DataBaseHelper dh = new DataBaseHelper();
        try {
            dh.register(patient);
        } catch (IOException | ClassNotFoundException | SQLException ex) {
            ex.printStackTrace(System.err);
        }

    }

    public void fillComboBox() {
        try {
            DataBaseHelper dh = new DataBaseHelper();
            List<Profession> list = dh.ListWork();
            for (Profession work : list) {
                cmbProfessionDoctor.getItems().addAll(work);
            }

        } catch (IOException | ClassNotFoundException | SQLException ex) {
            ex.printStackTrace(System.err);
        }
    }
}
