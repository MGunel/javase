package hospitalmanagement;

import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.validation.RequiredFieldValidator;
import hms.pojos.Admin;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import validation.Validation;

/**
 * FXML Controller class
 *
 * @author GUNEL
 */
public class AdminLoginController implements Initializable {

    @FXML
    private JFXTextField txtUsername;
    @FXML
    private JFXPasswordField txtPassword;
    @FXML
    private Label lblEmail;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        RequiredFieldValidator rqval = new RequiredFieldValidator();
        txtUsername.getValidators().add(rqval);

        rqval.setMessage("Field is empty");

        txtUsername.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (!newValue) {
                    txtUsername.validate();
                }

            }
        });
        txtPassword.getValidators().add(rqval);

        rqval.setMessage("Field is empty");
        txtPassword.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (!newValue) {
                    txtPassword.validate();
                }

            }
        });
        HospitalManagement.yaddas.put("loginPage", this);
    }

    public JFXTextField getTxtUsername() {
        return txtUsername;
    }

    public void setTxtUsername(JFXTextField txtUsername) {
        this.txtUsername = txtUsername;
    }

    public JFXPasswordField getTxtPassword() {
        return txtPassword;
    }

    public void setTxtPassword(JFXPasswordField txtPassword) {
        this.txtPassword = txtPassword;
    }

    @FXML
    private void clickSignUp(ActionEvent event) {
    }

    @FXML
    private void adminLogin() {
        if (Validation.isValidEmailAddress(txtUsername.getText())) {
            lblEmail.setVisible(false);
            DataBaseHelper dh = new DataBaseHelper();
            Alert alert = new Alert(Alert.AlertType.ERROR);
            try {
                Admin admin = dh.findAdminByUsername(txtUsername.getText());
                if (admin == null) {
                    alert.setTitle("Error");
                    alert.setContentText("You dont have account please Sign up");
                    alert.showAndWait();
                    System.out.println("Bele birisi yoxdur");
                } else {

                    if (!(admin.getPassword().equals(txtPassword.getText()))) {
                        alert.setTitle("Message");
                        alert.setAlertType(Alert.AlertType.INFORMATION);
                        alert.setContentText("Your password is wrong");
                        alert.showAndWait();

                    } else {
                        if (admin.getStatus().name().equals("ACTIVE")) {

                            Parent parent = FXMLLoader.load(getClass().getResource("/hospitalmanagement/AdminHomePage.fxml"));
                            Stage stage = new Stage();
                            Scene sceen = new Scene(parent);
                            stage.setScene(sceen);
                            stage.show();
                            System.out.println("Admin Home Page");
                        } else {
                            alert.setTitle("Message");
                            alert.setAlertType(Alert.AlertType.INFORMATION);
                            alert.setContentText("Your account must be approved by admins.Wait for,please...  ");
                            alert.showAndWait();
                        }
                    }

                }
            } catch (IOException | SQLException e) {
                e.printStackTrace(System.err);
            }
        } else {
            lblEmail.setVisible(true);
        }
    }

    public Admin findLoginedAdminId() {
        DataBaseHelper dh = new DataBaseHelper();
        try {
            Admin admin = dh.findAdminByUsername(txtUsername.getText());
            return admin;
        } catch (SQLException ex) {
            ex.printStackTrace(System.err);
        }
        return null;

    }

    @FXML
    public void adminSignUp() throws IOException {

        Parent parent = FXMLLoader.load(getClass().getResource("/hospitalmanagement/AdminSignUp.fxml"));
        Stage stage = new Stage();
        Scene scene = new Scene(parent);
        stage.setScene(scene);
        stage.show();

    }

}
