package hospitalmanagement;

import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.validation.RequiredFieldValidator;
import hms.pojos.Doctor;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.stage.Stage;
import static javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import validation.Validation;

/**
 * FXML Controller class
 *
 * @author Gunel
 */
public class DoctorLoginController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        RequiredFieldValidator rqval = new RequiredFieldValidator();
        txtUsername.getValidators().add(rqval);
        
        rqval.setMessage("Field is empty");
        
        txtUsername.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (!newValue) {
                    txtUsername.validate();
                }
                
            }
        });
        txtPassword.getValidators().add(rqval);
        
        rqval.setMessage("Field is empty");
        txtPassword.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (!newValue) {
                    txtPassword.validate();
                }
                
            }
        });
        HospitalManagement.yaddas.put("loginPage", this);
        System.out.println("Hello***************");
        
    }
    
    @FXML
    public void clickSignUp() throws IOException {
        
        Parent parent = FXMLLoader.load(getClass().getResource("/hospitalmanagement/DoctorSignUp.fxml"));
        Stage stage = new Stage();
        Scene scene = new Scene(parent);
        stage.setScene(scene);
        stage.show();
        
    }
    
    @FXML
    private JFXTextField txtUsername;
    
    @FXML
    private JFXPasswordField txtPassword;
    @FXML
    private Label lblEmail;
    
    public JFXTextField getTxtUsername() {
        return txtUsername;
    }
    
    public void setTxtUsername(JFXTextField txtUsername) {
        this.txtUsername = txtUsername;
    }
    
    public JFXPasswordField getTxtPassword() {
        return txtPassword;
    }
    
    public void setTxtPassword(JFXPasswordField txtPassword) {
        this.txtPassword = txtPassword;
    }
    
    @FXML
    public void doctorLogin(ActionEvent event) {
        
        if (Validation.isValidEmailAddress(txtUsername.getText())) {
            lblEmail.setVisible(false);
            DataBaseHelper dh = new DataBaseHelper();
            Alert alert = new Alert(AlertType.ERROR);
            try {
                Doctor doctor = dh.findDoctorByUsername(txtUsername.getText());
                if (doctor == null) {
                    alert.setTitle("Error");
                    alert.setContentText("You dont have account please Sign up");
                    alert.showAndWait();
                    System.out.println("Bele birisi yoxdur");
                } else {
                    
                    if (!(doctor.getPassword().equals(txtPassword.getText()))) {
                        alert.setTitle("Message");
                        alert.setAlertType(AlertType.INFORMATION);
                        alert.setContentText("Your password is wrong");
                        alert.showAndWait();
                        
                    } else {
                        //  ((Node) event.getSource()).getScene().getWindow().hide();

                        Parent parent = FXMLLoader.load(getClass().getResource("/hospitalmanagement/DoctorHomePage.fxml"));
                        Stage stage = new Stage();
                        Scene sceen = new Scene(parent);
                        stage.setScene(sceen);
                        stage.show();
                        System.out.println("Doctors Home Page");
                    }
                    
                }
            } catch (IOException | SQLException e) {
                e.printStackTrace(System.err);
            }
        } else {
            lblEmail.setVisible(true);
        }
    }
    
    public Doctor findLoginedDoctorId() {
        DataBaseHelper dh = new DataBaseHelper();
        try {
            Doctor doctor = dh.findDoctorByUsername(txtUsername.getText());
            return doctor;
        } catch (SQLException ex) {
            ex.printStackTrace(System.err);
        }
        return null;
        
    }
    
}
