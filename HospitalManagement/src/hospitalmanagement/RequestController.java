package hospitalmanagement;

import com.jfoenix.controls.JFXComboBox;
import hms.pojos.AvailableTime;
import hms.pojos.Doctor;
import hms.pojos.Patient;
import hms.pojos.Profession;
import hms.pojos.Request;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;

/**
 * FXML Controller class
 *
 * @author GUNEL
 */
public class RequestController implements Initializable {

    /**
     * Initializes the controller class.
     */
    //silecem
    public PatientLoginController page = (PatientLoginController) HospitalManagement.yaddas.get("patientLoginPage");

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        fillProfessionToComboBox();

        // listen for changes to the doctors combo box selection accordingly.
        comboBoxProfession.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Profession>() {
            @Override
            public void changed(ObservableValue<? extends Profession> selected, Profession oldProf, Profession newProf) {
                if (newProf != null) {
                    fillDoctorsToComboBox();
                }
            }
        });
        comboBoxDoctors.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Doctor>() {
            @Override
            public void changed(ObservableValue<? extends Doctor> selected, Doctor oldDoctor, Doctor newDoctor) {
                if (newDoctor != null) {
                    listViewTime.getItems().clear();
                    fillTimesToListView();
                }
            }
        });
    }
    @FXML
    private ListView<AvailableTime> listViewTime;

    @FXML
    private TextArea textAreaReason;

    @FXML
    private JFXComboBox<Profession> comboBoxProfession;

    @FXML
    private JFXComboBox<Doctor> comboBoxDoctors;

    @FXML
    public void clickSendRequest() {

        try {
            if (listViewTime.getSelectionModel().getSelectedItem() != null) {

                DataBaseHelper dh = new DataBaseHelper();
                Request request = new Request();
                Patient p = new Patient();
                p.setPatientId(page.findLoginedPatientId().getPatientId());
                request.setPatient(p);
                Doctor doctor = new Doctor();
                doctor.setDoctorId(comboBoxDoctors.getSelectionModel().getSelectedItem().getDoctorId());
                request.setDoctor(doctor);
                request.setReason(textAreaReason.getText());
                request.setAppointmentDate(listViewTime.getSelectionModel().getSelectedItem().getAvailable_date());
                dh.insertRequest(request);
            }
        } catch (SQLException e) {
            e.printStackTrace(System.err);
        }
    }

    public void fillProfessionToComboBox() {

        try {
            DataBaseHelper dh = new DataBaseHelper();
            List<Profession> profession = dh.ListWork();
            comboBoxProfession.getItems().addAll(profession);
        } catch (IOException | ClassNotFoundException | SQLException e) {
            e.printStackTrace(System.err);
        }
    }

    public void fillDoctorsToComboBox() {
        listViewTime.getItems().clear();
        comboBoxDoctors.getItems().clear();
        try {
            DataBaseHelper dh = new DataBaseHelper();
            Long profession_id = comboBoxProfession.getSelectionModel().getSelectedItem().getId();
            List<Doctor> doctors = dh.findDoctorsByProfessionId(profession_id);
            comboBoxDoctors.getItems().addAll(doctors);
        } catch (SQLException e) {
            e.printStackTrace(System.err);

        }
    }

    public void fillTimesToListView() {
        try {
            DataBaseHelper dh = new DataBaseHelper();
            Long doctorId = comboBoxDoctors.getSelectionModel().getSelectedItem().getDoctorId();
            List<AvailableTime> times = dh.findAvailableTimesByDoctorId(doctorId);
            listViewTime.getItems().addAll(times);
        } catch (SQLException e) {
            e.printStackTrace(System.err);
        }
    }

}
