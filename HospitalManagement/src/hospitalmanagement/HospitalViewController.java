package hospitalmanagement;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * FXML Controller class
 *
 * @author user
 */
public class HospitalViewController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        DataBaseHelper dh = new DataBaseHelper();
        try {
            dh.connect();
            dh.disconnect();
        } catch (IOException | ClassNotFoundException | SQLException e) {
            e.printStackTrace(System.err);
        }

        showClock();

    }

    @FXML
    public void btnClick() {
        System.exit(0);
        System.out.println("CLIKED");
    }

    public void btnCheck() {

    }
    @FXML
    private Label lblClock;
    @FXML
    private Button btnPatient;

    @FXML
    private Button btnDoctor;

    @FXML
    private Button btnAdmin;

    @FXML
    private Button btnHome;

    @FXML
    private Button btnExit;

    @FXML
    private Button btnInfo;

    @FXML
    public void showClock() {
        // Calendar cal = new GregorianCalendar();

//        int second = cal.get(Calendar.SECOND);
//        int minute = cal.get(Calendar.MINUTE);
//        int hour = cal.get(Calendar.HOUR);
        Thread t = new Thread() {

            public void run() {
                while (true) {
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
//                            DateFormat dt = new SimpleDateFormat("hh:mm:ss");
//                            Date date = new Date();
//                            lblClock.setText(dt.format(date));
                            Calendar cal = new GregorianCalendar();

                            int second = cal.get(Calendar.SECOND);
                            int minute = cal.get(Calendar.MINUTE);
                            int hour = cal.get(Calendar.HOUR);
                            int AM_PM = cal.get(Calendar.AM_PM);
                            String fixed_hour = "";
                            if (hour < 10) {
                                fixed_hour = "0" + hour;
                            } else {
                                fixed_hour = "" + hour;
                            }
                            String fixed_min = "";
                            if (minute < 10) {
                                fixed_min = "0" + minute;
                            } else {
                                fixed_min = "" + minute;
                            }
                            String fixed_sec = "";
                            if (second < 10) {
                                fixed_sec = "0" + second;
                            } else {
                                fixed_sec = "" + second;
                            }
                            if (AM_PM == 0) {
                                lblClock.setText(fixed_hour + ":" + fixed_min + ":" + fixed_sec + " AM");
                            } else {
                                lblClock.setText(fixed_hour + ":" + fixed_min + ":" + fixed_sec + " PM");
                            }
                        }
                    });
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ex) {
                        ex.printStackTrace(System.err);
                    }
                }
            }

        };
        t.start();
    }
    public static int y;

    @FXML
    public void clickAdmin() {
        try {
            Parent parent = FXMLLoader.load(getClass().getResource("/hospitalmanagement/AdminLogin.fxml"));
            Stage stage = new Stage();
            Scene sceen = new Scene(parent);
            stage.setScene(sceen);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace(System.err);
        }

    }

    @FXML
    public void clickDoctor() {
        try {
            Parent parent = FXMLLoader.load(getClass().getResource("/hospitalmanagement/DoctorLogin.fxml"));
            Stage stage = new Stage();
            Scene sceen = new Scene(parent);
            stage.setScene(sceen);
            stage.setOnCloseRequest(e -> {
                System.exit(0);
            });
            stage.show();
        } catch (IOException e) {
            e.printStackTrace(System.err);
        }
    }

    @FXML
    public void clickPatient() {
        try {
            Parent parent = FXMLLoader.load(getClass().getResource("/hospitalmanagement/PatientLogin.fxml"));
            Stage stage = new Stage();
            Scene sceen = new Scene(parent);
            stage.setScene(sceen);
            stage.setOnCloseRequest(e -> {
                System.exit(0);
            });
            stage.show();
        } catch (IOException e) {
            e.printStackTrace(System.err);
        }

    }

    @FXML
    public void goDoctorLog() {

        try {
            Parent parent = FXMLLoader.load(getClass().getResource("/hospitalmanagement/DoctorLogin.fxml"));
            Stage stage = new Stage();
            Scene sceen = new Scene(parent);
            stage.setScene(sceen);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace(System.err);
        }
    }

    @FXML
    public void clickInfo() {
        try {
            Parent parent = FXMLLoader.load(getClass().getResource("/hospitalmanagement/Information.fxml"));
            Scene scene = new Scene(parent);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.initStyle(StageStyle.UNDECORATED);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace(System.err);
        }
    }

}
