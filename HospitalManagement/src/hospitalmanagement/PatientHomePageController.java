package hospitalmanagement;

import com.jfoenix.controls.JFXRadioButton;
import hms.pojos.Gender;
import hms.pojos.Patient;
import hms.pojos.Picture;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.imageio.ImageIO;

/**
 * FXML Controller class
 *
 * @author GUNEL
 */
public class PatientHomePageController implements Initializable {

    /**
     * Initializes the controller class.
     */
    public static PatientLoginController page = (PatientLoginController) HospitalManagement.yaddas.get("patientLoginPage");
//Stage window;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //    window = (Stage) txtUsername.getScene().getWindow();
        //    window.setHeight(258.0);

        refreshPatientHome();
    }
    @FXML
    private TextField txtpassword;

    @FXML
    private TextField txtUsername;

    @FXML
    private TextField txtSurname;

    @FXML
    private TextField txtContact;

    @FXML
    private TextField txtName;

    @FXML
    private TextField txtAddress;

    @FXML
    private Button choose;

    @FXML
    private TextField txtGender;

    @FXML
    private TextField txtAge;

    @FXML
    private ImageView imgProfil;

    @FXML
    private JFXRadioButton rdbMale;

    @FXML
    private JFXRadioButton rdbFemale;

    public void refreshPatientHome() {
        DataBaseHelper dh = new DataBaseHelper();
        try {
            Patient patient = dh.patientLoadPage(page.getTxtUsername().getText());
            txtUsername.setText(patient.getUsername());
            txtpassword.setText(patient.getPassword());
            txtName.setText(patient.getName());
            txtSurname.setText(patient.getSurname());
            if (patient.getGender().name().equals("MALE")) {
                rdbMale.setSelected(true);
            }
            if (patient.getGender().name().equals("FEMALE")) {
                rdbFemale.setSelected(true);
            }
            txtAge.setText(String.valueOf(patient.getAge()));
            txtContact.setText(patient.getContact());
            txtAddress.setText(patient.getAddress());
            String path = patient.getImage();
            InputStream imageIn = new FileInputStream(
                    new File(path));
            Image myImage = new Image(imageIn);
            imgProfil.setImage(myImage);

        } catch (IOException | ClassNotFoundException | SQLException e) {
            e.printStackTrace(System.err);
        }

    }

    Picture picture = new Picture();

    @FXML
    public void selectImage() {

        FileChooser.ExtensionFilter imagefilter = new FileChooser.ExtensionFilter("Image Files", "*.jpg", "*.png");
        FileChooser fc = new FileChooser();
        fc.getExtensionFilters().add(imagefilter);
        File file = fc.showOpenDialog(null);
        try {
            BufferedImage buf_img = ImageIO.read(file);
            Image image = SwingFXUtils.toFXImage(buf_img, null);
            picture.setImgName(file.getAbsolutePath());
            imgProfil.setImage(image);
        } catch (IOException e) {
        }
    }

    @FXML
    public void showMoreInfo() throws InterruptedException {
        Stage window = (Stage) txtUsername.getScene().getWindow();
//        for (double i = 258.0; i < 700.0; i++) {
//            window.setHeight(i);
//            TimeUnit.MILLISECONDS.sleep(1);
//        }

        //arti
        Thread t = new Thread() {

            public void run() {

                while (true) {

                    Platform.runLater(new Runnable() {
                        double i = 258.0;

                        @Override
                        public void run() {

                            if (i < 900.0) {
                                window.setHeight(i);
                                i += 10;
                            }
                        }
                    });
                    try {
                        Thread.sleep(1000);
                    } catch (Exception ex) {
                        ex.printStackTrace(System.err);
                    }
                }
            }

        };
        t.start();
    }

    @FXML
    public void makeAppointment() {
        try {
            Parent parent = FXMLLoader.load(getClass().getResource("/hospitalmanagement/Request.fxml"));
            Stage stage = new Stage();
            Scene scene = new Scene(parent);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace(System.err);
        }
    }

    @FXML
    public void btnUpdatePatient() {

        try {
            DataBaseHelper dh = new DataBaseHelper();
            Patient patient = new Patient();
            patient.setUsername(txtUsername.getText());
            patient.setPassword(txtpassword.getText());
            patient.setName(txtName.getText());
            patient.setSurname(txtSurname.getText());
            if (rdbMale.isSelected()) {
                patient.setGender(Gender.MALE);
            }
            if (rdbFemale.isSelected()) {
                patient.setGender(Gender.FEMALE);
            }
            patient.setAge(Integer.parseInt(txtAge.getText()));
            patient.setImage(picture.getImgName());
            patient.setContact(txtContact.getText());
            patient.setAddress(txtAddress.getText());

            dh.update(patient);
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Information Dialog");
            alert.setHeaderText(null);
            alert.setContentText("Your information was uptaded,succesfully!");
            alert.showAndWait();
        } catch (IOException | ClassNotFoundException | NumberFormatException | SQLException e) {
            e.printStackTrace(System.err);
        }
    }

    @FXML
    public void logOut(ActionEvent event) {
        try {

            ((Node) event.getSource()).getScene().getWindow().hide();

        } catch (Exception e) {
            e.printStackTrace(System.err);
        }
    }

    @FXML
    public void goToShowRequests() {
        try {
            Parent parent = FXMLLoader.load(getClass().getResource("/hospitalmanagement/ShowRequestsByPatient.fxml"));
            Scene scene = new Scene(parent);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.show();
            
          
        } catch (IOException e) {
            e.printStackTrace(System.err);
        }

    }

}
