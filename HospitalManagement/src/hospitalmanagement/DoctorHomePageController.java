package hospitalmanagement;

import com.jfoenix.controls.JFXRadioButton;
import hms.pojos.Doctor;
import hms.pojos.Gender;
import hms.pojos.Picture;
import hms.pojos.Profession;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.imageio.ImageIO;

/**
 * FXML Controller class
 *
 * @author GUNEL
 */
public class DoctorHomePageController implements Initializable {

    /**
     * Initializes the controller class.
     */
    public static DoctorLoginController page = (DoctorLoginController) HospitalManagement.yaddas.get("loginPage");

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        fillComboBox();
        refreshDoctorHome();

    }
    @FXML
    private TextField txtimg;
    @FXML
    private TextField txtpassword;

    public TextField getTxtUsername() {
        return txtUsername;
    }

    public void setTxtUsername(TextField txtUsername) {
        this.txtUsername = txtUsername;
    }

    @FXML
    private TextField txtUsername;

    @FXML
    private TextField txtSurname;

    @FXML
    private TextField txtContact;

    @FXML
    private TextField txtName;

    @FXML
    private TextField txtAddress;

    @FXML
    private TextField txtExperience;

    @FXML
    private ComboBox<Profession> cmbProfession;

    @FXML
    private TextField txtAge;

    @FXML
    private ImageView imgProfil;

    @FXML
    private JFXRadioButton rdbMale;

    @FXML
    private JFXRadioButton rdbFemale;

    public void refreshDoctorHome() {
        DataBaseHelper dh = new DataBaseHelper();
        try {
            Doctor doctor = dh.doctorLoadPage(page.getTxtUsername().getText());
            txtUsername.setText(doctor.getUsername());
            txtpassword.setText(doctor.getPassword());
            txtName.setText(doctor.getName());
            txtSurname.setText(doctor.getSurname());
            if (doctor.getGender().name().equals("MALE")) {
                rdbMale.setSelected(true);
            }
            if (doctor.getGender().name().equals("FEMALE")) {
                rdbFemale.setSelected(true);
            }
            txtAge.setText(String.valueOf(doctor.getAge()));
            txtContact.setText(doctor.getContact());
            txtAddress.setText(doctor.getAddress());
            txtExperience.setText(String.valueOf(doctor.getExperience()));
            Long l = doctor.getProfession().getId() - 1;
            cmbProfession.getSelectionModel().select(l.intValue());
            String path = doctor.getImage();
            InputStream imageIn = new FileInputStream(
                    new File(path));
            Image myImage = new Image(imageIn);
            imgProfil.setImage(myImage);

        } catch (ClassNotFoundException | SQLException | IOException ex) {
            Logger.getLogger(DoctorHomePageController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void fillComboBox() {
        try {
            DataBaseHelper dh = new DataBaseHelper();
            List<Profession> list = dh.ListWork();
            for (Profession work : list) {
                cmbProfession.getItems().addAll(work);
            }

        } catch (IOException | ClassNotFoundException | SQLException ex) {
            ex.printStackTrace(System.err);
        }
    }
    Picture picture = new Picture();

    @FXML
    public void selectImage() {

        FileChooser.ExtensionFilter imagefilter = new FileChooser.ExtensionFilter("Image Files", "*.jpg", "*.png");
        FileChooser fc = new FileChooser();
        fc.getExtensionFilters().add(imagefilter);
        File file = fc.showOpenDialog(null);
        try {
            BufferedImage buf_img = ImageIO.read(file);
            Image image = SwingFXUtils.toFXImage(buf_img, null);
            picture.setImgName(file.getAbsolutePath());
            imgProfil.setImage(image);
        } catch (IOException e) {
            e.printStackTrace(System.err);
        }
    }

    public void showPicture() {
        try {
            DataBaseHelper dh = new DataBaseHelper();
            Picture sekil = dh.dbShowPicture("eli@mail.ru");
            String path = sekil.getImgName();
            InputStream imageIn = new FileInputStream(
                    new File(path));
            Image myImage = new Image(imageIn);
            imgProfil.setImage(myImage);
        } catch (IOException | ClassNotFoundException | SQLException e) {

        }
    }

    @FXML
    public void btnUpdate() {
        try {
            DataBaseHelper dh = new DataBaseHelper();
            Doctor doctor = new Doctor();
            doctor.setUsername(txtUsername.getText());
            doctor.setPassword(txtpassword.getText());
            doctor.setName(txtName.getText());
            doctor.setSurname(txtSurname.getText());
            if (rdbMale.isSelected()) {
                doctor.setGender(Gender.MALE);
            }
            if (rdbFemale.isSelected()) {
                doctor.setGender(Gender.FEMALE);
            }
            doctor.setAge(Integer.parseInt(txtAge.getText()));
            //doctor.setImage("C:\\Users\\user\\Downloads\\adminPanel.png.png");
            doctor.setImage(picture.getImgName());
            doctor.setContact(txtContact.getText());
            doctor.setAddress(txtAddress.getText());
            doctor.setExperience(Double.parseDouble(txtExperience.getText()));
            Profession prof = new Profession();
            prof.setId(cmbProfession.getSelectionModel().getSelectedItem().getId());
            doctor.setProfession(prof);
            dh.update(doctor);

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Information Dialog");
            alert.setHeaderText(null);
            alert.setContentText("Your information was uptaded,succesfully!");
            alert.showAndWait();

        } catch (IOException | ClassNotFoundException | NumberFormatException | SQLException e) {
            System.err.println(e);
        }
    }

    @FXML
    public void showRequests() {
        try {
            Parent parent = FXMLLoader.load(getClass().getResource("/hospitalmanagement/SubmittedRequest.fxml"));
            Stage stage = new Stage();
            Scene scene = new Scene(parent);
            stage.setScene(scene);
            stage.show();
        } catch (IOException ex) {
            System.err.println(ex.getLocalizedMessage());
        }

    }

    @FXML
    public void logOut(ActionEvent event) {
        try {
            ((Node) event.getSource()).getScene().getWindow().hide();

        } catch (Exception e) {
            e.printStackTrace(System.err);
        }
    }

}
