package hospitalmanagement;

import com.jfoenix.controls.JFXRadioButton;
import hms.pojos.Gender;
import hms.pojos.Patient;
import hms.pojos.UserStatus;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author user
 */
public class PatientSignUpController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }   
    @FXML
    private TextField txtUsername;

    @FXML
    private TextField txtSurname;

    @FXML
    private TextField txtContact;

    @FXML
    private TextField txtName;

    @FXML
    private TextField txtAddress;

    @FXML
    private JFXRadioButton rdbMale;

    @FXML
    private JFXRadioButton rdbFemale;

    @FXML
    private TextField txtAge;

    @FXML
    private PasswordField txtPassword;
    
    @FXML
    public void registerAll() {
        Patient patient = new Patient();
        patient.setName(txtName.getText());
        patient.setSurname(txtSurname.getText());
        patient.setUsername(txtUsername.getText());
        patient.setPassword(txtPassword.getText());
        patient.setStatus(UserStatus.ACTIVE);
        if (rdbMale.isSelected()) {
            patient.setGender(Gender.MALE);
        }
        if (rdbFemale.isSelected()) {
            patient.setGender(Gender.FEMALE);
        }
        patient.setAge(Integer.parseInt(txtAge.getText()));
        patient.setContact(txtContact.getText());
        patient.setAddress(txtAddress.getText());
        DataBaseHelper dh = new DataBaseHelper();
        try {
            dh.register(patient);
        } catch (IOException | ClassNotFoundException | SQLException ex) {
            ex.printStackTrace(System.err);
        }

    }
}
