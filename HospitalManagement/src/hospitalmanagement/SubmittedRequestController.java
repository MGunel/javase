package hospitalmanagement;

import hms.pojos.Doctor;
import hms.pojos.Request;
import hms.pojos.RequestStatus;
import java.net.URL;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * FXML Controller class
 *
 * @author GUNEL
 */
public class SubmittedRequestController implements Initializable {

    @FXML
    private TableColumn<Request, Date> columnAppointmentDate;
    @FXML
    private TableView<Request> tblSubmittedRequestTime;
    @FXML
    private Label lblNameSurname;
    @FXML
    private TextArea txtSubmittedReason;
    @FXML
    private Label lblStatus;

    public static DoctorLoginController page = (DoctorLoginController) HospitalManagement.yaddas.get("loginPage");

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        loadDateToTable();
        txtSubmittedReason.setEditable(false);
        tblSubmittedRequestTime.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                fillTxtReason();
            }
        });
    }

    public void loadDateToTable() {
        try {
            DataBaseHelper dh = new DataBaseHelper();
            Doctor doctor = dh.findDoctorByUsername(page.getTxtUsername().getText());
            System.out.println(doctor);
            Long doctorId = doctor.getDoctorId();
            System.out.println(doctorId);
            ObservableList<Request> dateList = FXCollections.observableArrayList(dh.findAllRequestsByDoctorId(doctorId));
            columnAppointmentDate.setCellValueFactory(new PropertyValueFactory<>("appointmentDate"));
            tblSubmittedRequestTime.setItems(null);
            tblSubmittedRequestTime.setItems(dateList);
        } catch (SQLException ex) {
            ex.printStackTrace(System.err);
        }
    }

    public void fillTxtReason() {
        lblNameSurname.setText(tblSubmittedRequestTime.getSelectionModel().getSelectedItem().getPatient().getName()
                + " " + tblSubmittedRequestTime.getSelectionModel().getSelectedItem().getPatient().getSurname());
        txtSubmittedReason.setText(tblSubmittedRequestTime.getSelectionModel().getSelectedItem().getReason());
        lblStatus.setText(tblSubmittedRequestTime.getSelectionModel().getSelectedItem().getStatus().name());

        if (lblStatus.getText().equals("PENDING")) {

            lblStatus.setStyle("-fx-text-fill:#5180cc");
        } else if (lblStatus.getText().equals("ACCEPTED")) {

            lblStatus.setStyle("-fx-text-fill:green");
        } else if (lblStatus.getText().equals("CANCELLED")) {

            lblStatus.setStyle("-fx-text-fill:red");
        }
    }
    DataBaseHelper dh = new DataBaseHelper();

    @FXML
    public void acceptRequest() {
        if (lblStatus.getText().equals("PENDING")) {
            try {
                Request request = new Request();
                request.setRequestId(tblSubmittedRequestTime.getSelectionModel().getSelectedItem().getRequestId());
                request.setStatus(RequestStatus.ACCEPTED);
                dh.updateRequest(request);
                System.out.println("Succesfully accepted");
                loadDateToTable();
                lblStatus.setStyle("-fx-text-fill:green");
                lblStatus.setText("ACCEPTED");
            } catch (SQLException ex) {
                ex.printStackTrace(System.err);
            }
        }

    }

    @FXML
    public void cancelRequest() {
        if (lblStatus.getText().equals("PENDING")) {

            try {
                Request request = new Request();
                request.setRequestId(tblSubmittedRequestTime.getSelectionModel().getSelectedItem().getRequestId());
                request.setStatus(RequestStatus.CANCELLED);
                dh.updateRequest(request);
                System.out.println("Succesfully cancelled");
                loadDateToTable();
                lblStatus.setStyle("-fx-text-fill:red");
                lblStatus.setText("CANCELLED");
            } catch (SQLException ex) {
                ex.printStackTrace(System.err);
            }
        }
    }

}
