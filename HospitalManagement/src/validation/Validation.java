package validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author GUNEL
 */
public class Validation {
    public static boolean isValidEmailAddress(String email) {
        boolean status = false;
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        Pattern p = Pattern.compile(ePattern);
        Matcher m = p.matcher(email);
        //m.matches() &&
        if (m.find() && m.group().equals(email)) {

            status = true;
        } else {
            status = false;
        }
        return status;
    }
}
